This is a rewrite of my [transposer](https://sourceforge.net/projects/transposer/) project.

There are two main differences between the project hosted on sourceforge.
Instead of writing it in QT/C++, or Lazarus, this project is in C#, and an as yet undetermined GUI toolkit. The other thing is that this project is using NUnit for testing. The plan is to make this project using a TDD approach.