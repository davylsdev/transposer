using NUnit.Framework;
using System;
using Transposer;

namespace TransposerTest
{
	[TestFixture()]
	public class TransposerTest
	{
		[Test()]
		public void ScaleTest ()
		{
			Scale sourceScale = new Scale ();
			sourceScale.FirstNote = "A";

			Assert.AreEqual ("A", sourceScale.FirstNote);
		}
	
	}
}